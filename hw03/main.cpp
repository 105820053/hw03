#include <gtest/gtest.h>
#include "vector.h"
#include <string>
#include <math.h>
using namespace std;

int main(int argc , char **argv)
{
    testing :: InitGoogleTest( &argc , argv ) ;
    return RUN_ALL_TESTS();
}
TEST(addVectors,common1)
{
    double a[2]={1,1},b[2]={2,3};
    Vector v(a,2);
    Vector w(b,2);

    ASSERT_EQ(3 , addVectors(&v,&w)->component(1));
    ASSERT_EQ(4 , addVectors(&v,&w)->component(2));
};
TEST (addVectors,abnormal)//for wrong dimension
{
    double a[2]={5,8}, b[3]={13,21,34};
    Vector v(a,2);
    Vector w(b,3);
    try{
        ASSERT_ANY_THROW(addVectors(&v,&w));
    }catch (string s) {
        ASSERT_EQ(string("wrong dim"),s);
    }

};
TEST(substractVectors,common1)
{
    double a[2]={55,89},b[2]={144,233};
    Vector v(a,2);
    Vector w(b,2);

    ASSERT_EQ(-89 , substractVectors(&v,&w)->component(1));
    ASSERT_EQ(-144 , substractVectors(&v,&w)->component(2));
};
TEST (substractVectors,abnormal)//for wrong dimension
{
    double a[2]={377,610}, b[3]={987,1597,2584};
    Vector v(a,2);
    Vector w(b,3);
    try{
        ASSERT_ANY_THROW(substractVectors(&v,&w));
    }catch (string s) {
        ASSERT_EQ(string("wrong dim"),s);
    }

};
TEST(multiplyScalar,common1)
{
    double a[2]={4181,6765},b=2;
    Vector v(a,2);


    ASSERT_EQ(8362 , multiplyScalar(b,&v)->component(1));
    ASSERT_EQ(13530 , multiplyScalar(b,&v)->component(2));
};
TEST(getUnitVector,common1)
{
    double a[2]={3,4};
    Vector v(a,2);


    ASSERT_EQ( 0.6 , getUnitVector(&v)->component(1));
    ASSERT_EQ( 0.8 , getUnitVector(&v)->component(2));
};
