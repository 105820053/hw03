#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED
#include<math.h>
#include <string>
using namespace std;
class Vector
{
    public:
        Vector(double a[], int dimension)
        {
            dim=dimension;
            v=new double [dim];
            for(int i=0;i<dim;++i)
            {
                v[i]=a[i];
            }
        }
        ~Vector()
        {
            delete [] v;
        }
        int dimension()
        {
            return dim;
        }
        double component (int dimension)
        {
            return v[dimension-1];
        }
        private:
        int dim;
        double *v;

};

Vector* addVectors(Vector* a,Vector *b)
{

    if(a->dimension()!=b->dimension())
    {
        throw string("wrong dim");
    }
    double v[a->dimension()];
    for(int i=1;i<=a->dimension();i++)
    {
        v[i-1]=a->component(i)+b->component(i);

    }
    Vector* result=new Vector(v,a->dimension());
    return result;
}




Vector* substractVectors(Vector* a,Vector* b)
{

    if(a->dimension()!=b->dimension())
    {
        throw string("wrong dim");
    }
    double v[a->dimension()];
    for(int i=1;i<=a->dimension();i++)
    {
        v[i-1] = (a->component(i))-(b->component(i));
    }
    Vector* result=new Vector(v,a->dimension());
    return result;
}

Vector* multiplyScalar(double u,Vector* a)
{
    double v[a->dimension()];

    for(int i=1;i<=a->dimension();i++)
    {
        v[i-1]=a->component(i)*u;
    }

    Vector* result=new Vector(v,a->dimension());
    return result;
}

Vector* getUnitVector(Vector* a)
{

    double v[a->dimension()];



    double time;
    double length;
    for(int i=1;i<=a->dimension();i++)
    {
        time=time+pow(a->component(i),2);
    }
    length=sqrt(time);

    for(int i=1;i<=a->dimension();i++)
    {
        v[i-1]=a->component(i)/length;
    }
    Vector* result=new Vector(v,a->dimension());
    return result;
}

#endif // VECTOR_H_INCLUDED

